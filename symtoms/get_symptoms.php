<?php


$response = array();



require_once  '../db_connection.php';


$db = new DB_CONNECT();

if (isset($_GET["pid"]) & isset($_GET["lang"])) {
    $response["symptoms"] = array();
    $response["symptom_translations"] = array();
    $language = $_GET['lang'];
    $result = mysql_query("SELECT * FROM symptoms where symptomfield  = '" . $_GET['pid'] . "'") or die(mysql_error());


    if (mysql_num_rows($result) > 0) {

        $symptom_translation = array();
        $symptom = array();
        while ($row = mysql_fetch_array($result)) {
            $symptom["symptomfield"] = $row["symptomfield"];
            $symptom["symptomName"] = urlencode(utf8_decode($row["symptomName"]));
            $symptom_translation["img"] = urlencode($row["img"]);
            $symptom["symptom_ID"] = $row["symptom_ID"];
            
            array_push($response["symptoms"], $symptom);
            $result2 = mysql_query("SELECT * FROM symptom_translations where symptom  =  ".$symptom["symptom_ID"]." and lang = '".$language."'") or die(mysql_error());

            if (mysql_num_rows($result2) > 0) {
           
                $translation = mysql_fetch_array($result2);
                 
                $symptom_translation["lang"] = $translation["lang"];
                if($language == "es" || $language == "fr"){
                    $symptom_translation["symptom_translation"] = urlencode(utf8_decode($translation["symptom_translation"]));
                } else{
                    $symptom_translation["symptom_translation"] = urlencode($translation["symptom_translation"]);   
                }
                $symptom_translation["symptom"] =$translation["symptom"];
                $symptom_translation["symptomtransl_ID"] = $translation["symptomtransl_ID"];

       
                array_push($response["symptom_translations"], $symptom_translation);
            } else {

                $response["success"] = 0;
                $response["message"] = "No symptoms found";

                echo json_encode($response);
            }
        }

        $response["success"] = 1;

        echo json_encode($response);
    } else {

        $response["success"] = 0;
        $response["message"] = "No symptoms found";

        echo json_encode($response);
    }
}


